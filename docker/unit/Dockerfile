# Get basic debian sid system and setup locales
FROM debian:sid
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get autoclean && apt-get clean && apt-get -y update && apt-get -y install sudo apt-utils locales
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen en_US.UTF-8 && dpkg-reconfigure locales && /usr/sbin/update-locale LANG=en_US.UTF-8
ENV LC_ALL en_US.UTF-8
# Setup "runner" user
RUN adduser --disabled-password --gecos '' --ingroup staff runner
RUN adduser runner sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
USER runner
ENV DEBIAN_FRONTEND=noninteractive
RUN mkdir /home/runner/jhbuild
WORKDIR /home/runner/jhbuild
RUN sudo apt-get install -y git make gettext python
# Setup git proxy if http_proxy exists
RUN if [ -n "${http_proxy}" ]; then git config --global http.proxy $http_proxy; fi
# Install jhbuild
RUN git clone --depth=1 https://git.gnome.org/browse/jhbuild
WORKDIR jhbuild
RUN ./autogen.sh --simple-install && make && make install
RUN mkdir -p /home/runner/bin
RUN ln -sf /home/runner/.local/bin/jhbuild /home/runner/bin/jhbuild
RUN chmod a+x /home/runner/bin/jhbuild
ENV PATH=/home/runner/bin:${PATH}
# Set up git user
RUN git config --global user.name "runner"
RUN git config --global user.email "runner"
# Install gitproxy tool and configure if necessary
ADD gitproxy /home/runner/bin
RUN if [ -n "${http_proxy}" ]; then git config --global core.gitproxy /home/runner/bin/gitproxy; fi
# small fix for jhbuild to be able to use sticky_date on modules that don't use the master branch
# and another fix to allow modifications to checked out code when using sticky dates
ADD jhbuild_sticky_date.patch /home/runner
RUN cd /home/runner/jhbuild/jhbuild && git apply ~/jhbuild_sticky_date.patch
# Set fixed date for jhbuild
RUN mkdir /home/runner/.config
RUN cp /home/runner/jhbuild/jhbuild/examples/sample.jhbuildrc /home/runner/.config/jhbuildrc
ENV STICKY_DATE='2017-09-14'
RUN echo "sticky_date='${STICKY_DATE}'" >> /home/runner/.config/jhbuildrc
# Disable WebKit building as it is long and slow
RUN echo "skip=['WebKit']" >> /home/runner/.config/jhbuildrc
# Set the shell variable, necessary for mozjs
ENV SHELL=/bin/sh
# Disable manual on colord due to bug
RUN echo "module_mesonargs['colord']='-Denable-man=false'" >> /home/runner/.config/jhbuildrc
# Configure the keyboard
RUN echo "keyboard-configuration  keyboard-configuration/variant  select  English (US)" | sudo debconf-set-selections
# Disable documentation and tests on libinput
RUN echo "module_mesonargs['libinput']='-Dtests=false -Ddocumentation=false'" >> /home/runner/.config/jhbuildrc
# Set bash as the default shell, as some build steps need this
# RUN echo "dash dash/sh boolean false" | sudo debconf-set-selections
# RUN sudo DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash
# Patch for mutter where the unit tests where not working
# RUN jhbuild updateone -D ${STICKY_DATE} mutter
# ADD libmutter.patch /home/runner
# RUN cd /home/runner/jhbuild/checkout/mutter && git apply ~/libmutter.patch
# Install dependencies
RUN sudo apt-get update
RUN sudo apt-get -y install libtool automake libxslt-dev pkgconf itstool intltool libxml2-dev xsltproc libxml2-utils docbook-xsl docbook-xml docbook libsource-highlight-dev libffi-dev zlib1g-dev libpcre3-dev flex bison python3-dev libcairo2-dev libsystemd-dev libpolkit-gobject-1-dev libgraphviz-dev valac libdbus-1-dev libgnutls28-dev libp11-kit-dev libproxy-dev libsqlite3-dev ninja-build g++ ragel libxft-dev libjpeg-dev libxtst-dev libxi-dev libxkbcommon-dev libxkbcommon-x11-dev doxygen xmlto libepoxy-dev libegl1-mesa-dev desktop-file-utils libgcrypt20-dev python-six libdbus-glib-1-dev libwebkit2gtk-4.0-dev liboauth-dev gperf libical-dev libkrb5-dev libldap2-dev cmake libdb-dev libusb-1.0-0-dev libgudev-1.0-dev libpam0g-dev libcanberra-gtk-dev libplymouth-dev libcanberra-gtk3-dev libxkbfile-dev libudev-dev libseccomp-dev docbook-utils liblcms2-dev argyll ppp-dev libavahi-client-dev libavahi-glib-dev libnl-route-3-dev libndp-dev libnl-genl-3-dev libreadline-dev uuid-dev iptables libpolkit-agent-1-dev xserver-xorg-input-wacom libcups2-dev libpulse-dev libmtdev-dev check libunwind-dev libevdev-dev libstartup-notification0-dev libgbm-dev xwayland libxcb-res0-dev mesa-common-dev libgl1-mesa-dev libxklavier-dev libcaribou-dev xvfb libarchive-dev libyaml-dev libexiv2-dev libexif-dev libiw-dev texinfo
# Build modules
RUN jhbuild build adwaita-icon-theme
RUN jhbuild build dconf
RUN jhbuild build accountsservice
RUN jhbuild build gdm
RUN jhbuild build gnome-backgrounds
RUN jhbuild build gnome-color-manager
RUN jhbuild build clutter-gtk
RUN jhbuild build gnome-online-accounts
RUN jhbuild build gnome-settings-daemon
RUN jhbuild build libgtop
RUN jhbuild build sound-theme-freedesktop
RUN jhbuild build gmime3
RUN jhbuild build totem-pl-parser
RUN jhbuild build grilo
RUN jhbuild build gnome-bluetooth
RUN jhbuild build mobile-broadband-provider-info
RUN jhbuild build network-manager-applet
RUN jhbuild build graphene
RUN jhbuild build gst-plugins-bad
RUN jhbuild build gst-plugins-good
RUN jhbuild build clutter-gst
RUN jhbuild build gnome-video-effects
RUN jhbuild build cheese
RUN jhbuild build libpinyin
RUN jhbuild build ibus-libpinyin
RUN jhbuild build ibus-anthy
RUN jhbuild build ibus-hangul
RUN jhbuild build libgnomekbd
RUN jhbuild build gnome-control-center
RUN jhbuild build gnome-getting-started-docs
RUN jhbuild build gcr
RUN jhbuild build gnome-keyring
RUN jhbuild build gnome-session
RUN jhbuild build uhttpmock
RUN jhbuild build libgdata
RUN jhbuild build evolution-data-server
RUN jhbuild build mozjs52
RUN jhbuild build gjs
RUN jhbuild build zenity
RUN jhbuild build mutter
RUN jhbuild build telepathy-logger
RUN jhbuild build gnome-shell
RUN jhbuild build gnome-menus
RUN jhbuild build gnome-shell-extensions
RUN jhbuild build cantarell-fonts
RUN jhbuild build gnome-themes-standard
RUN jhbuild build gnome-user-docs
RUN jhbuild build gvfs
RUN jhbuild build mousetweaks
RUN jhbuild build pyatspi2
RUN jhbuild build speech-dispatcher
RUN jhbuild build orca
RUN jhbuild build gssdp
RUN jhbuild build gupnp
RUN jhbuild build gupnp-av
RUN jhbuild build gupnp-dlna
RUN jhbuild build libgee
RUN jhbuild build tracker
RUN jhbuild build libmediaart
RUN jhbuild build rygel
RUN jhbuild build telepathy-mission-control
RUN jhbuild build vino
RUN jhbuild build gnome-initial-setup
RUN jhbuild build gnome-autoar
RUN jhbuild build libgrss
RUN jhbuild build libgsf
RUN jhbuild build tracker-miners
RUN jhbuild build nautilus
RUN jhbuild build gnome-user-share
RUN jhbuild build meta-gnome-core-shell
RUN jhbuild build baobab
RUN jhbuild build libpeas
RUN jhbuild build eog
RUN jhbuild build libhttpseverywhere
RUN jhbuild build epiphany
RUN jhbuild build libgxps
RUN jhbuild build evince
RUN jhbuild build file-roller
RUN jhbuild build gtksourceview-3
RUN jhbuild build gspell
RUN jhbuild build gedit
RUN jhbuild build gnome-calculator
RUN jhbuild build gnome-calendar
RUN jhbuild build gnome-characters
RUN jhbuild build gsound
RUN jhbuild build gnome-clocks
RUN jhbuild build folks
RUN jhbuild build libchamplain
RUN jhbuild build gnome-contacts
RUN jhbuild build libgfbgraph
RUN jhbuild build libzapojit
RUN jhbuild build gnome-online-miners
RUN jhbuild build libgepub
RUN jhbuild build gnome-documents
RUN jhbuild build gnome-font-viewer
RUN jhbuild build gnome-maps
RUN jhbuild build gom
RUN jhbuild build grilo-plugins
RUN jhbuild build gnome-music
RUN jhbuild build babl
RUN jhbuild build gegl
RUN jhbuild build gnome-photos
RUN jhbuild build gnome-screenshot
RUN jhbuild build fwupd
RUN jhbuild build gnome-software
RUN jhbuild build mm-common
RUN jhbuild build libsigcplusplus-2.0
RUN jhbuild build cairomm-1.0
RUN jhbuild build glibmm-2.4
RUN jhbuild build pangomm-1.4
RUN jhbuild build atkmm-1.6
RUN jhbuild build gtkmm-3
RUN jhbuild build gnome-system-monitor
RUN jhbuild build gnome-terminal
RUN jhbuild build gnome-todo
RUN jhbuild build gnome-weather
RUN jhbuild build sushi
RUN jhbuild build totem
RUN jhbuild build yelp
RUN jhbuild build simple-scan
RUN jhbuild build gnome-disk-utility
RUN jhbuild build gtk-vnc
RUN jhbuild build libgovirt
RUN jhbuild build osinfo-db-tools
RUN jhbuild build libosinfo
RUN jhbuild build libvirt-glib
RUN jhbuild build phodav
RUN jhbuild build spice-protocol
RUN jhbuild build spice-gtk
RUN jhbuild build gnome-boxes
RUN jhbuild build gnome-logs
RUN jhbuild build meta-gnome-core-utilities
RUN jhbuild build meta-gnome-core
# RUN jhbuild build meson
# RUN jhbuild build glib
# RUN jhbuild build --nodeps vala
# RUN jhbuild build
# ADD run_js_test.sh /home/runner/bin
# RUN cp /home/runner/jhbuild/install/lib/gnome-shell/*.so /home/runner/jhbuild/install/lib
# RUN cp /home/runner/.cache/jhbuild/build/gnome-shell/src/run-js-test /home/runner/bin
